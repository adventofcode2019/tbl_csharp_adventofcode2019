using System.Diagnostics;
using System;
using System.Linq;
using System.Collections.Generic;

namespace adventofcode2019
{
    public class DayThree : ISolver
    {
        public int SolvePuzzle(string input, PuzzleIndex index)
        {
            var wires = ProcessInput(input);
            if (index == PuzzleIndex.One) return SolvePuzzle1(wires.firstWire, wires.secondWire);
            return SolvePuzzle2(wires.firstWire, wires.secondWire);
        }

        private static (Dictionary<(int, int), int> firstWire, Dictionary<(int, int), int> secondWire) ProcessInput(string input)
        {
            var wires = input.Split(Environment.NewLine);
            var wire1 = ProcessWire(wires[0]);
            var wire2 = ProcessWire(wires[1]);
            return (wire1, wire2);
        }

        public static int SolvePuzzle1(Dictionary<(int, int), int> wire1, Dictionary<(int, int), int> wire2)
        {
            var closestIntersection = wire1.Keys.Intersect(wire2.Keys)
                .Min(x => Math.Abs(x.Item1) + Math.Abs(x.Item2));
            return closestIntersection;
        }
        public static int SolvePuzzle2(Dictionary<(int, int), int> wire1, Dictionary<(int, int), int> wire2)
        {
            var firstIntersection = wire1.Keys.Intersect(wire2.Keys)
                .Min(x => wire1[x] + wire2[x]);
            return firstIntersection;
        }

        public static Dictionary<(int, int), int> ProcessWire(string moves)
        {
            var dict = new Dictionary<(int, int), int>();
            int x = 0, y = 0, step = 0;

            foreach (var move in moves.Split(","))
            {
                var dist = int.Parse(move.Substring(1));
                for (var i = 0; i < dist; i++)
                    if (move[0] == 'U')
                        dict.TryAdd((x, ++y), ++step);
                    else if (move[0] == 'D')
                        dict.TryAdd((x, --y), ++step);
                    else if (move[0] == 'L')
                        dict.TryAdd((--x, y), ++step);
                    else if (move[0] == 'R')
                        dict.TryAdd((++x, y), ++step);
            }

            return dict;
        }
    }
}
