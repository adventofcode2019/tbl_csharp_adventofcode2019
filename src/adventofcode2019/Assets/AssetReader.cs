using System.Collections.Generic;
using System;
using System.IO;

namespace Assets
{
    public class AssetReader
    {
        public string ReadAsset(int adventDay)
        {
            if (adventDay < 1 || adventDay > 25) throw new IndexOutOfRangeException("adventday must be between 1-25");
            var filename = Filenames[adventDay - 1];
            return File.ReadAllText($"Assets/{filename}");
        }

        private static string[] Filenames = new string[]{
            "Day1Input.txt",
            "Day2Input.txt",
            "Day3Input.txt",
            "Day4Input.txt",
            "Day5Input.txt",
            "Day6Input.txt",
            "Day7Input.txt",
            "Day8Input.txt",
            "Day9Input.txt",
            "Day10Input.txt",
            "Day11Input.txt",
            "Day12Input.txt",
            "Day13Input.txt",
            "Day14Input.txt",
            "Day15Input.txt",
            "Day16Input.txt",
            "Day17Input.txt",
            "Day18Input.txt",
            "Day19Input.txt",
            "Day20Input.txt",
            "Day21Input.txt",
            "Day22Input.txt",
            "Day23Input.txt",
            "Day24Input.txt",
            "Day25Input.txt"
        };

    }
}
