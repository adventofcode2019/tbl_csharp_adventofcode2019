using System;
using CommandLine;

namespace adventofcode2019
{
    public class CommandLineOptions
    {
        [Option('d', "day", Required = false, HelpText = "Select day of advent to run.")]
        public int Day { get; set; }
    }
}