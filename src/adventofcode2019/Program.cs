﻿using System.Runtime.CompilerServices;
using System.Linq;
using System.Diagnostics;
using System;
using CommandLine;
using Assets;
[assembly: InternalsVisibleTo("adventofcode2019.test")]
namespace adventofcode2019
{
    class Program
    {
        private static AssetReader assetReader = new AssetReader();

        private static ISolver[] Puzzles = new ISolver[]{
            new DayOne(),
            new DayTwo(),
            new DayThree(),
        };
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<CommandLineOptions>(args)
                   .WithParsed<CommandLineOptions>(o =>
                   {
                       if (o.Day <= 0) RunAllPuzzles();
                       else RunPuzzleForDay(o.Day, new Stopwatch());
                   });
        }

        private static void RunAllPuzzles()
        {
            PrintStart();
            var timer = new Stopwatch();
            var fullTimer = new Stopwatch();
            fullTimer.Start();
            for (int i = 0; i < Puzzles.Count(); i++)
            {
                RunPuzzleForDay(i, timer);
            }

            fullTimer.Stop();
            PrintFullTimingStats(fullTimer);

        }

        private static void RunPuzzleForDay(int dayIndex, Stopwatch timer)
        {
            var arrayIndex = dayIndex - 1;
            var input = assetReader.ReadAsset(dayIndex);
            timer.Restart();
            var res = Puzzles[arrayIndex].SolvePuzzle(input, PuzzleIndex.One);
            timer.Stop();
            PrintResult(dayIndex, PuzzleIndex.One, res);
            PrintTimerStats(dayIndex, PuzzleIndex.One, timer);

            timer.Restart();
            res = Puzzles[arrayIndex].SolvePuzzle(input, PuzzleIndex.Two);
            timer.Stop();
            PrintResult(dayIndex, PuzzleIndex.Two, res);
            PrintTimerStats(dayIndex, PuzzleIndex.One, timer);
        }

        private static void PrintTimerStats(int day, PuzzleIndex index, Stopwatch sw)
        {
            Console.WriteLine("Execution of Day: {0} for Puzzle {1} took ms: {2}", day, index, sw.ElapsedMilliseconds);
            Console.WriteLine();
        }
        private static void PrintFullTimingStats(Stopwatch sw)
        {
            Console.WriteLine("Execution af all tests took ms: {0}", sw.ElapsedMilliseconds);
            Console.WriteLine();
        }

        private static void PrintStart()
        {
            Console.WriteLine("STARTING PUZZLE PROCESSING");
            Console.WriteLine("");
        }

        private static void PrintPuzzleStart(int day)
        {
            Console.WriteLine("STARTING PUZZLE FOR DAY: {0}", day);
            Console.WriteLine("");
        }

        private static void PrintResult(int day, PuzzleIndex index, int result)
        {
            Console.WriteLine("Result of Day: {0} for Puzzle {1} is: {2}", day, index, result);
            Console.WriteLine("");
        }

    }
}
