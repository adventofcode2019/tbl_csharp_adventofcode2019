using System;

namespace adventofcode2019
{
    public interface ISolver
    {
        int SolvePuzzle(string input, PuzzleIndex index);
    }
}