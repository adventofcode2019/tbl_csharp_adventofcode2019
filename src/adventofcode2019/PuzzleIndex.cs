using System;

namespace adventofcode2019
{
    public enum PuzzleIndex
    {
        One = 1,
        Two = 2
    }
}
