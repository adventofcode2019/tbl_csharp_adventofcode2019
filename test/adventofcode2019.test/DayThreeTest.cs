using System;
using Xunit;
using adventofcode2019;
using FluentAssertions;
using System.Collections.Generic;

namespace adventofcode2019.test
{
    public class DayThreeTest
    {
        [Theory]
        [MemberData(nameof(Data))]
        public void FirstPuzzle_SingleValueTest(string input, int expectation)
        {
            //ARRANGE
            var p = new DayThree();
            //ACT
            var res = p.SolvePuzzle(input, PuzzleIndex.One);
            //ASSERT
            res.Should().Be(expectation);

        }
        public static IEnumerable<object[]> Data =>
        new List<object[]>
        {
            new object[] { $"R8,U5,L5,D3{Environment.NewLine}U7,R6,D4,L4", 6 },
            new object[] { $"R75,D30,R83,U83,L12,D49,R71,U7,L72{Environment.NewLine}U62,R66,U55,R34,D71,R55,D58,R83", 159 },
            new object[] { $"R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51{Environment.NewLine}U98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 135 },
        };

        // [Theory]
        // [InlineData(12, 14, 1969, 100756, 34241)]
        // public void DayOne_FirstPuzzle_MultiValueTest(int mass1, int mass2, int mass3, int mass4, int expectation)
        // {
        //     //ARRANGE
        //     var d1 = new DayOne();
        //     var inputs = new string[] { mass1.ToString(), mass2.ToString(), mass3.ToString(), mass4.ToString() };
        //     //ACT
        //     var res = d1.SolvePuzzle(string.Join(Environment.NewLine, inputs), PuzzleIndex.One);
        //     //ASSERT
        //     res.Should().Be(expectation);

        // }

        // [Theory]
        // [InlineData(12, 2)]
        // [InlineData(14, 2)]
        // [InlineData(1969, 966)]
        // [InlineData(100756, 50346)]
        // public void DayOne_SecondPuzzle_SingleValueTest(int mass, int expectation)
        // {
        //     //ARRANGE
        //     var d1 = new DayOne();
        //     //ACT
        //     var res = d1.SolvePuzzle(mass.ToString(), PuzzleIndex.Two);
        //     //ASSERT
        //     res.Should().Be(expectation);

        // }

        // [Theory]
        // [InlineData(12, 14, 1969, 100756, 51316)]
        // public void DayOne_SecondPuzzle_MultiValueTest(int mass1, int mass2, int mass3, int mass4, int expectation)
        // {
        //     //ARRANGE
        //     var d1 = new DayOne();
        //     var inputs = new string[] { mass1.ToString(), mass2.ToString(), mass3.ToString(), mass4.ToString() };
        //     //ACT
        //     var res = d1.SolvePuzzle(string.Join(Environment.NewLine, inputs), PuzzleIndex.Two);
        //     //ASSERT
        //     res.Should().Be(expectation);

        // }
    }
}
