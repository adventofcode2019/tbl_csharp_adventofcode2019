using System;
using Xunit;
using adventofcode2019;
using FluentAssertions;
using System.Collections.Generic;

namespace adventofcode2019.test
{
    public class DayTwoTest
    {
        [Theory]
        [MemberData(nameof(Data))]
        public void FirstPuzzle_SingleValueTest(int[] input, int[] expectation)
        {
            //ARRANGE
            var p = new DayTwo();
            //ACT
            var res = p.PuzzleOneAlgorithm(input);
            //ASSERT
            res.Should().Equal(expectation);

        }
        public static IEnumerable<object[]> Data =>
        new List<object[]>
        {
            new object[] { new int[]{1,0,0,0,99}, new int[]{2,0,0,0,99} },
            new object[] { new int[]{2,3,0,3,99}, new int[]{2,3,0,6,99} },
            new object[] { new int[]{2,4,4,5,99,0}, new int[]{2,4,4,5,99,9801} },
            new object[] { new int[]{1,1,1,4,99,5,6,0,99}, new int[]{30,1,1,4,2,5,6,0,99} },
        };

        // [Theory]
        // [InlineData(12, 14, 1969, 100756, 34241)]
        // public void DayOne_FirstPuzzle_MultiValueTest(int mass1, int mass2, int mass3, int mass4, int expectation)
        // {
        //     //ARRANGE
        //     var d1 = new DayOne();
        //     var inputs = new string[] { mass1.ToString(), mass2.ToString(), mass3.ToString(), mass4.ToString() };
        //     //ACT
        //     var res = d1.SolvePuzzle(string.Join(Environment.NewLine, inputs), PuzzleIndex.One);
        //     //ASSERT
        //     res.Should().Be(expectation);

        // }

        // [Theory]
        // [InlineData(12, 2)]
        // [InlineData(14, 2)]
        // [InlineData(1969, 966)]
        // [InlineData(100756, 50346)]
        // public void DayOne_SecondPuzzle_SingleValueTest(int mass, int expectation)
        // {
        //     //ARRANGE
        //     var d1 = new DayOne();
        //     //ACT
        //     var res = d1.SolvePuzzle(mass.ToString(), PuzzleIndex.Two);
        //     //ASSERT
        //     res.Should().Be(expectation);

        // }

        // [Theory]
        // [InlineData(12, 14, 1969, 100756, 51316)]
        // public void DayOne_SecondPuzzle_MultiValueTest(int mass1, int mass2, int mass3, int mass4, int expectation)
        // {
        //     //ARRANGE
        //     var d1 = new DayOne();
        //     var inputs = new string[] { mass1.ToString(), mass2.ToString(), mass3.ToString(), mass4.ToString() };
        //     //ACT
        //     var res = d1.SolvePuzzle(string.Join(Environment.NewLine, inputs), PuzzleIndex.Two);
        //     //ASSERT
        //     res.Should().Be(expectation);

        // }
    }
}
