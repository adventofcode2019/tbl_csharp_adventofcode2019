using System;
using Xunit;
using adventofcode2019;
using FluentAssertions;

namespace adventofcode2019.test
{
    public class DayOneTest
    {
        [Theory]
        [InlineData(12, 2)]
        [InlineData(14, 2)]
        [InlineData(1969, 654)]
        [InlineData(100756, 33583)]
        public void DayOne_FirstPuzzle_SingleValueTest(int mass, int expectation)
        {
            //ARRANGE
            var d1 = new DayOne();
            //ACT
            var res = d1.SolvePuzzle(mass.ToString(), PuzzleIndex.One);
            //ASSERT
            res.Should().Be(expectation);

        }

        [Theory]
        [InlineData(12, 14, 1969, 100756, 34241)]
        public void DayOne_FirstPuzzle_MultiValueTest(int mass1, int mass2, int mass3, int mass4, int expectation)
        {
            //ARRANGE
            var d1 = new DayOne();
            var inputs = new string[] { mass1.ToString(), mass2.ToString(), mass3.ToString(), mass4.ToString()};
            //ACT
            var res = d1.SolvePuzzle(string.Join(Environment.NewLine, inputs),  PuzzleIndex.One);
            //ASSERT
            res.Should().Be(expectation);

        }

        [Theory]
        [InlineData(12, 2)]
        [InlineData(14, 2)]
        [InlineData(1969, 966)]
        [InlineData(100756, 50346)]
        public void DayOne_SecondPuzzle_SingleValueTest(int mass, int expectation)
        {
            //ARRANGE
            var d1 = new DayOne();
            //ACT
            var res = d1.SolvePuzzle(mass.ToString(), PuzzleIndex.Two);
            //ASSERT
            res.Should().Be(expectation);

        }

        [Theory]
        [InlineData(12, 14, 1969, 100756, 51316)]
        public void DayOne_SecondPuzzle_MultiValueTest(int mass1, int mass2, int mass3, int mass4, int expectation)
        {
            //ARRANGE
            var d1 = new DayOne();
            var inputs = new string[] { mass1.ToString(), mass2.ToString(), mass3.ToString(), mass4.ToString()};
            //ACT
            var res = d1.SolvePuzzle(string.Join(Environment.NewLine, inputs),  PuzzleIndex.Two);
            //ASSERT
            res.Should().Be(expectation);

        }
    }
}
